<?php 
/* C:\xampp\htdocs\TastyIgniter\themes\tastyigniter-orange/_pages/local\reviews.blade.php */
class Pagic609128c74a5cc104341687_b650d52f6d2916794f99151d092c7f6dClass extends \Main\Template\Code\PageCode
{

public function onStart()
{
    if (!View::shared('showReviews')) {
        flash()->error(lang('igniter.local::default.review.alert_review_disabled'))->now();

        return Redirect::to($this->controller->pageUrl($this['localReview']->property('redirectPage')));
    }
}

}
